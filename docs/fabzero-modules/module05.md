# 5. Dynamique de groupe et projet final
 

## projet 1 : création d'arbres à problèmes et à solutions

Durant cette semaine, je me suis attelé à créer ce que nous appelons un ''arbre à problème'', mais également un ''arbre à solution''. A ces fins-ci, je me suis inspiré du projet ''pHduino'', un gadget servant à la mesure de la concentration en cations hydronium de l'eau. un problème écologique qui m'a fait écho, suite à la lecture de la présentation en ligne de ce projet, était l'acidification des océans.

###    Arbre à problèmes

Cette arbre à pour tronc l'acidification des océans, en racines, les causes sous-jacentes et en guise de branches / feuilles les conséquences

![image](Arbre1.png)

### Arbre à solutions

Cette arbre à une fonction contraire que le précedent. En tronc, nous nous focalisons sur la thématique de la stabilisation du pH océaniques, en racines, les moyens d'y parvenir et en branches / feuilles, les bénéfices à y retirer.

![image](Arbre2.jpg)
